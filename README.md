# **coremcs** -- Core Monte Carlo Simulation framework #

The `coremcs` package provides the core (model-independent) functionality of the Distributed Monte Carlo Simulation (Dist-MCS) framework. The software is currently in alpha release.

See the derivative packages, [gtapmcs](http://bitbucket.org/plevin/gtapmcs) and [gcammcs](http://bitbucket.org/plevin/gcammcs), for use with the GTAP and GCAM models, respectively.

### How do I get set up? ###

Presently, there is no proper setup script. See the user documentation in `coremcs/doc/Distributed-MCS-guide-v1.0.pdf`

This package requires several Python packages, including the following. The version tested appears in parentheses, though other versions may work:

* lxml (3.4.2)
* matplotlib (1.4.3)
* numexpr (2.3.1)
* numpy (1.9.2)
* openpyxl (2.0.2)
* pandas (0.16.0)
* pycopg2 (2.6 -- if you choose to use PostgreSQL rather than sqlite3)
* scipy (0.15.1)
* seaborn (0.6 -- for plotting only)
* sqlalchemy (1.0.2)

### Contribution guidelines ###

* Later for that... in the meantime, contact me (see below.)

### Who do I talk to? ###

* Contact Richard Plevin (rich@plevin.com) for more information