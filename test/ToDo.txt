- Analysis: create tests for plotting and data export
    - Create a dummy sqlite3 database that can be used as the data source?

- Test gtapmcs by creating a dummy app that uses it...
    - Modify to use addColumns rather than gtapmcs.sql

ILUCMCS
- Make sure it works out-of-the-box in terms of distros and params.
- Lots to reconsider in Package.py

Documentation
- All configuration parameters at each level
- Guide to subclassing Package/Configuration/etc.



