'''
Created on Dec 26, 2012

@author: SAM FENDELL
'''
import unittest

from os import path
from coremcs.DistGenerate import csvToDiscreteDistro
from coremcs.Constants import DISCRETE_SUFFIX, COUNT_TITLE, DEFAULT_BINS


class Test(unittest.TestCase):

    TEST_DATA_DIR = 'DistGenerateTestData'
    DATA_TITLE = 'data'

    def fileToSet(self, fileName):
        lines = set(open(fileName, 'r').readlines())
        return frozenset(frozenset(line.split()) for line in lines)

    def convertFileTest(self, fileName, varTitles=None, bins=DEFAULT_BINS):
        varTitles = varTitles or ['A', 'B']
        TEST_DATA_DIR = Test.TEST_DATA_DIR
        DATA_TITLE = Test.DATA_TITLE
        inputFile = path.join(TEST_DATA_DIR, fileName + '.csv')
        outputFile = path.join(TEST_DATA_DIR, fileName + 'Test.' + DISCRETE_SUFFIX)
        targetFile = path.join(TEST_DATA_DIR, fileName + 'Target.' + DISCRETE_SUFFIX)
        csvToDiscreteDistro(inputFile, outputFile, DATA_TITLE, varTitles=varTitles, countTitle=COUNT_TITLE, bins=bins)
        assert self.fileToSet(outputFile) == self.fileToSet(targetFile)

    def testCSVToDiscreteDistro(self):
        self.convertFileTest('TestStandard')
        self.convertFileTest('TestIdentical')
        self.convertFileTest('TestSmall', varTitles=['A'])
        self.convertFileTest('Test2bins', varTitles=[r'LongTitle', 'we\'ird:title../?!@#%', 'other\t\t\ttitle with tabs and stuff'], bins=2)
