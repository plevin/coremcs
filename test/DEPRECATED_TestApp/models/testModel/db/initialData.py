INITIAL_DATA = {'code': [{'codeId'      :-27,
                          'codeName'    : 'testNewCode',
                          'description' : 'This is a test code.'},
                         {'codeId'      : 8,
                          'codeName'    : 'testOtherNewCodeNoDescription'}],
                
                'experiment': [{'expName'     : 'newExpName',
                                'description' : 'Check out this cool new experiment!'},
                               {'expName'     : 'Another cool experiment, added right now!',
                                'description' : 'I was lying, that experiment sucks'}],
                
		         'submodel' : [{'name' : 'submodelA'},
			                   {'name' : 'submodelB'}]
                }

