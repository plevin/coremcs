'''
Created 8/3/2014

@author: Rich Plevin
'''
import os, logging
from unittest import TestCase
from coremcs.util import loadModuleFromPath, loadObjectFromPath, importFromDotSpec # , setupSysLogger
from coremcs.Configuration import getConfigManager, CoreConfiguration, DEFAULT_SECTION, APP_NAME_OPTION

APP_NAME = 'mcstest'

class TestModuleLoading(TestCase):

    @classmethod
    def setUpClass(cls):
        cfg = getConfigManager(CoreConfiguration)
        cfg.parser.set(DEFAULT_SECTION, APP_NAME_OPTION, APP_NAME)
        _logger = logging.getLogger('coremcs.Utilities')
        # setupSysLogger(_logger)


    def test_loadModuleFromSpec(self):
        testDir = 'ModuleLoadTestData'

        # Test loading file that's neither .py nor .pyc
        modulePath = os.path.join(testDir, 'TextFile.txt')
        self.assertRaises(Exception, loadModuleFromPath, modulePath)

        modulePath = os.path.join(testDir, 'Unloadable.py') # has a syntax error
        self.assertRaises(Exception, loadModuleFromPath, modulePath)


    def test_loadObjectFromSpec(self):
        testDir  = 'ModuleLoadTestData'
        function = 'random_function'

        modulePath = os.path.join(testDir, 'RandomPythonFile.py')
        fn = loadObjectFromPath('%s:%s' % (modulePath, function), None)
        self.assertEqual(fn(103), 103)

        modulePath = os.path.join(testDir, 'RandomPythonFile.pyc')
        fn = loadObjectFromPath('%s:%s' % (modulePath, function), None)
        self.assertEqual(fn('something'), 'something')

    def test_importFromDotSpec(self):
        fn = importFromDotSpec('coremcs.Constants')

