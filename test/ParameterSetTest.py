'''
Created on Jan 16, 2013

@author: Sam Fendell
@author: Richard Plevin

'''

import unittest, os
from coremcs.SymSet       import SymSet
from coremcs.Parameter import ParameterSet
from coremcs.MatrixParameter import MatrixParameter
from coremcs.Distro       import DistroGen
from coremcs.util    import findParamData
from coremcs.Database     import getDatabase, Input, Program, Sim

from coremcs.Package import CorePackage
from coremcs.Configuration import getConfigManager, CoreConfiguration, DEFAULT_SECTION, APP_NAME_OPTION

APP_NAME = 'mcstest'
DB_FILE  = 'test.sqlite'

def pathTo(f):
    return os.path.join(os.path.dirname(__file__), f)


def flatten(listOfLists):
    return [item for sublist in listOfLists for item in sublist]

# TODO: with changes to param, this might be unnecessary
def populateParams(parameterSet):
    '''
    Helper method that populates the param table with dummy parameters so that parameter set's stuff can work.
    '''
    return

    # db = getDatabase()
    # session = db.Session()
    #
    # for name in parameterSet.paramDict.keys():
    #     session.add(Input(programId=parameterSet.programId, paramName=name, description='desc'))
    #
    # session.commit()


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cfg = getConfigManager(CoreConfiguration)
        cfg.parser.set(DEFAULT_SECTION, APP_NAME_OPTION, APP_NAME)
        cfg.parser.set(APP_NAME, 'DbURL', 'sqlite:///%s' % DB_FILE)      # Force this into config parser
        CorePackage()

        # recreate the database
        if os.path.exists(DB_FILE):
            os.remove(DB_FILE)
        db = getDatabase()

        # Create an entry for this "program"
        if not db.getProgramId(APP_NAME):
            session = db.Session()
            session.add(Program(name=APP_NAME, description='Created for ParameterSetTest'))
            session.add(Sim(trials=10, description='Created for ParameterSetTest'))
            session.commit()

        DistroGen('testdist', lambda val: val)
        SymSet.readFile('./ParameterSetTestData/TestSets/paramSets.set')

    @classmethod
    def tearDownClass(cls):
        #db = getDatabase()
        #db.dropAll()
        pass            # we delete the DB in setUpClass, so no need for this


    def setUp(self):
        self.db = getDatabase()

    def newSim(self, trials=10):
        sim = Sim(trials=trials, description="test sim")
        session = self.db.Session()
        session.add(sim)
        session.commit()
        return sim.simId

    # TODO: this test should really be broken up into a bunch of smaller tests :( ohwell
    def testName(self):
        test = ParameterSet(APP_NAME,
                            pathTo('./ParameterSetTestData/TestParams/testParam1.prm'),
                            pathTo('ParameterSetTestdata/TestDistros/testDistro1.dst'),
                            MatrixParameter)
        trials = 10
        simId  = self.newSim(trials=trials)

        test.genTrials(trials)
        crosstrialTest = set()

        #populateParams(test)

        for trialNum in range(trials):
            trialData = test.getTrialData(simId, trialNum)      # get all the trialData data for this simId and trialNum
            data = findParamData(trialData, 'rowTarget')
            self.assertEqual(data[0][0], data[0][1])
            self.assertEqual(data[1][0], data[1][1])
            self.assertNotEquals(data[0][0], data[1][1])

            data = findParamData(trialData, 'noFactor')  # also tests the 'single' target
            val = data[0][0]
            crosstrialTest.add(val)

            for row in data:
                for item in row:
                    self.assertEqual(item, val)
                    assert item >= 0

            data = findParamData(trialData, 'colTarget')
            self.assertEqual(data[0][0], data[1][0])
            self.assertEqual(data[0][1], data[1][1])
            self.assertNotEquals(data[0][0], data[1][1])

            data = findParamData(trialData, 'cellsTarget')
            flatData = flatten(data)
            self.assertEqual(len(set(flatData)), len(flatData))

            data = findParamData(trialData, 'indexTarget')
            self.assertEqual(len(set(flatten(data))), 2)
            self.assertNotEquals(data[1][1], data[0][0])

            data = findParamData(trialData, 'rowIndexTarget')
            for row in data:
                self.assertEqual(len(set(row)), 1)

            data = findParamData(trialData, 'colIndexTarget')
            for x in range(3):
                self.assertEqual(data[0][x], data[1][x])
                self.assertEqual(data[1][x], data[2][x])
            self.assertEqual(len(set(flatten(data))), 2)

            data = findParamData(trialData, 'multiIndexTarget')
            self.assertEqual(len(set(flatten(data))), 6)
            self.assertEqual(data[0][1], data[1][0])
            self.assertEqual(data[0][1], 1)
            self.assertEqual(data[2][0], data[2][1])
            self.assertEqual(data[0][2], data[1][2])
            self.assertEqual(data[2][2], data[2][0] * data[0][2])

        self.assertEqual(len(crosstrialTest), trials)

    def testSymbolicParameters2dim(self):
        test = ParameterSet(APP_NAME,
                            './ParameterSetTestData/TestParams/testSetParams.prm',
                            './ParameterSetTestData/TestDistros/testSetParams.dst',
                            MatrixParameter)
        trials = 10
        simId  = self.newSim(trials=trials)

        #populateParams(test)

        test.genTrials(trials)
        for trialNum in range(trials):
            trialData = test.getTrialData(simId, trialNum)
            data = findParamData(trialData, 'param1')
            self.assertEqual([1, 1, 1], data[1])
            self.assertEqual([1, 1], data[0][1:])
            self.assertAlmostEqual(-0.5, data[0][0], delta=0.5)

    def testSymbolicParametersRowsOnly(self):
        test = ParameterSet(APP_NAME,
                            './ParameterSetTestData/TestParams/testSetParams.prm',
                            './ParameterSetTestData/TestDistros/testSetParams.dst',
                            MatrixParameter)
        trials = 10
        simId  = self.newSim(trials=trials)

        #populateParams(test)

        test.genTrials(trials)
        for trialNum in range(trials):
            trialData = test.getTrialData(simId, trialNum)
            data = findParamData(trialData, 'param2')
            self.assertEqual([1, 1, 1, 1], data[1])
            self.assertEqual([1, 1, 1, 1], data[2])
            self.assertEqual([1, 1], data[0][:2])
            self.assertEqual(1, data[0][-1])
            self.assertAlmostEqual(-0.5, data[0][2], delta=0.5)

    def testSymbolicParametersColsOnly(self):
        test = ParameterSet(APP_NAME,
                            './ParameterSetTestData/TestParams/testSetParams.prm',
                            './ParameterSetTestData/TestDistros/testSetParams.dst',
                            MatrixParameter)
        trials = 10
        simId  = self.newSim(trials=trials)

        #populateParams(test)
        test.genTrials(trials)

        for trialNum in range(trials):
            trialData = test.getTrialData(simId, trialNum)
            data = findParamData(trialData, 'param3')
            self.assertEqual([1, 1], data[1])
            self.assertEqual(1, data[0][0])
            self.assertAlmostEqual(-0.5, data[0][1], delta=0.5)


        # TODO: finish this
