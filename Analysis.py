'''
Created on 2013-02-06

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from . import log
from .error import CoreMcsSystemError, CoreMcsUserError
from .Database import getDatabase, getSession, Input
from .Configuration import getConfigManager
from .util import mkdirs

_logger = log.getLogger(__name__)


def makePlotPath(value, simId):
    cfg = getConfigManager()
    plotDir  = cfg.getParam('Core.PlotDir')
    subDir   = os.path.join(plotDir, "s%d" % simId)
    mkdirs(subDir)
    plotType = cfg.getParam('Core.PlotType')
    path = os.path.join(subDir, "%s.%s" % (value, plotType))
    #print "Plot path: ", path
    return path

def printExtraText(fig, text, loc='top', color='lightgrey', weight='ultralight', fontsize='xx-small'):
    """
    Print 'extra' text at the top, bottom, right, or left edge of the figure.
    """
    if not text:
        return

    rot = 0
    ha  = 'center'
    va  = 'center'
    x   = 0.5
    y   = 0.5

    if loc == 'top':
        y = 0.98
        va = 'top'
    elif loc == 'bottom':
        y = 0.02
        va = 'bottom'
    elif loc == 'right':
        x = 0.98
        ha = 'right'
        rot = 270
    else: # left
        x = 0.02
        ha = 'left'
        rot = 90

    fig.text(x, y, text, color=color, weight=weight, fontsize=fontsize, va=va, ha=ha, rotation=rot)


def plotHistogram(values, xlabel=None, ylabel=None, title=None,
                  extra=None, extraColor='grey', extraLoc='right',
                  hist=True, showCI=False, showMean=False, showMedian=False,
                  color=None, shade=False, kde=True, show=True, filename=None):

    fig = plt.figure()

    style    = "white"
    colorSet = "Set1"
    sns.set_style(style)
    sns.set_palette(colorSet, desat=0.6)
    red, blue, green, purple = sns.color_palette(colorSet, n_colors=4)

    color = blue if color is None else color
    count = values.count()
    bins  = count / 10 if count > 150 else (count / 5 if count > 50 else (count / 2 if count > 20 else None))
    sns.distplot(values, hist=hist, bins=bins, kde=kde, color=color, kde_kws={'shade': shade})

    sns.axlabel(xlabel=xlabel, ylabel=ylabel)
    sns.despine()

    if title:
        t = plt.title(title)
        t.set_y(1.02)

    printExtraText(fig, extra, color=extraColor, loc=extraLoc)

    if showCI or showMean:
        ymin, ymax = plt.ylim()
        xmin, xmax = plt.xlim()
        textSize = 9
        labely   = ymax * 0.95
        deltax   = (xmax-xmin) * 0.01

        if showCI:
            color = red
            ciLow  = np.percentile(values, 2.5)
            ciHigh = np.percentile(values, 97.5)
            plt.axvline(ciLow,  color=color, linestyle='solid', linewidth=2)
            plt.axvline(ciHigh, color=color, linestyle='solid', linewidth=2)
            plt.text(ciLow  + deltax, labely, '2.5%%=%.2f'  % ciLow,  size=textSize, rotation=90, color=color)
            plt.text(ciHigh + deltax, labely, '97.5%%=%.2f' % ciHigh, size=textSize, rotation=90, color=color)

        if showMean:
            color = green
            mean = np.mean(values)
            plt.axvline(mean, color=color, linestyle='solid', linewidth=2)
            plt.text(mean + deltax, labely, 'mean=%.2f' % mean, color=color, size=textSize, rotation=90)

        if showMedian:
            color = purple
            median = np.percentile(values, 50)
            labely = ymax * 0.50
            plt.axvline(median, color=color, linestyle='solid', linewidth=2)
            plt.text(median + deltax, labely, 'median=%.2f' % median, color=color, size=textSize, rotation=90)

    if show:
        plt.show()

    if filename:
        _logger.info("plotHistogram writing to: %s", filename)
        fig.savefig(filename)

    plt.close(fig)


def plotTornado(data, colname='value', labelsize=9, title=None, color=None, height=0.8,
                limit=20, rlabels=None, xlabel='Contribution to variance', figsize=None,
                show=True, filename=None, extra=None, extraColor='grey', extraLoc='right'):
    '''
    :param data: A sorted DataFrame or Series indexed by variable name, with
                 column named 'value' and if rlabels is set, a column of that
                 name holding descriptive labels to display.
    :param labelsize: font size for labels
    :param title: If not None, the title to show
    :param color: The color of the horizontal bars
    :param height: Bar height
    :param limit: The maximum number of variables to display
    :param rlabels: If not None, the name of a column holding values to show on the right
    :param xlabel: Label for X-axis
    :param figsize: tuple for desired figure size. Defaults to (12,6) if rlabels else (8,6).
    :param show: If True, the figure is displayed on screen
    :param filename: If not None, the figure is saved to this file
    :param extra: Extra text to display in the lower left corner of the plot
    :return: nothing
    '''
    count, cols = data.shape

    if 0 < limit < count:
        data = data[:limit]            # Truncate the DF to the top "limit" rows
        count = limit

    # Reverse the order so the larger (abs) values are at the top
    revIndex = list(reversed(data.index))
    data = data.loc[revIndex]

    itemNums = range(count)
    ypos = np.array(itemNums) - 0.08   # goose the values to better center labels

    if not figsize:
        figsize = (12, 6) if rlabels else (8, 6)

    #fig = plt.figure(figsize=figsize)
    #fig = plt.figure(facecolor='white', figsize=figsize)
    #plt.plot()

    # if it's a dataframe, we expect to find the data in the value column
    values = data if isinstance(data, pd.Series) else data[colname]

    if color is None:
        color = sns.color_palette("deep", 1)

    # TBD: This looks like it has been resolved; try this again using seaborn
    # tried pandas; most of the following manipulations can be handled in one call, but
    # it introduced an ugly dashed line at x=0 which I didn't see how to remove. Maybe
    # address this again if seaborn adds a horizontal bar chart.
    values.plot(kind="barh", color=sns.color_palette("deep", 1), figsize=figsize,
                xlim=(-1, 1), ylim=(-1, count), xticks=np.arange(-0.8, 1, 0.2))

    plt.xlabel(xlabel)

    right = 0.6 if rlabels else 0.9
    plt.subplots_adjust(left=0.3, bottom=0.1, right=right, top=0.9)  # more room for rlabels

    fig = plt.gcf()
    ax  = plt.gca()

    ax.xaxis.tick_top()
    ax.tick_params(axis='x', labelsize=labelsize)
    ax.tick_params(axis='y', labelsize=labelsize)
    ax.set_yticklabels(data.index)
    ax.set_yticks(itemNums)

    if rlabels:
        ax2 = plt.twinx()
        plt.ylim(-1, count)
        ax2.tick_params(axis='y', labelsize=labelsize)
        ax2.set_yticklabels(data[rlabels])
        ax2.set_yticks(itemNums)

        for t in ax2.xaxis.get_major_ticks() + ax2.yaxis.get_major_ticks():
            t.tick1On = False
            t.tick2On = False

    # show vertical grid lines only
    ax.yaxis.grid(False)
    ax.xaxis.grid(True)

    # Remove tickmarks from both axes
    for t in ax.xaxis.get_major_ticks() + ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False

    if title:
        plt.title(title, y=1.05)  # move title up to avoid tick labels

    printExtraText(fig, extra, loc=extraLoc, color=extraColor)

    if show:
        plt.show()

    if filename:
        _logger.debug("Saving tornado plot to %s" % filename)
        fig.savefig(filename)

    plt.close(fig)


def plotConvergence(simId, expName, paramName, values, show=True, save=False):
    '''
    Examine the first 3 moments (mean, std, skewness) in the data set
    for increasing number (N) of values, growing by the given increment.
    Optionally plot the relationship between each of the moments and N,
    so we can when (if) convergence occurs.
    '''
    _logger.debug("Generating convergence plots...")
    count = values.count()
    results = {'Mean': [], 'Stdev': [], 'Skewness': [], '95% CI': []}

    increment = min(100, count/10)
    nValues = range(increment, count, increment)

    for N in nValues:
        sublist = values[:N]
        results['Mean'].append(sublist.mean())
        results['Stdev'].append(sublist.std())
        results['Skewness'].append(sublist.skew())

        ciLow  = np.percentile(sublist, 2.5)
        ciHigh = np.percentile(sublist, 97.5)
        results['95% CI'].append(ciHigh - ciLow)

    # Insert zero value at position 0 for all lists to ensure proper scaling
    nValues.insert(0,0)
    for dataList in results.values():
        dataList.insert(0,0)

    labelsize=12
    for key, values in results.iteritems():
        plt.clf()   # clear previous figure
        ax = plt.gca()
        ax.tick_params(axis='x', labelsize=labelsize)
        ax.tick_params(axis='y', labelsize=labelsize)
        plt.plot(nValues, results[key])
        plt.title("%s" % paramName, size='large')
        ax.yaxis.grid(False)
        ax.xaxis.grid(True)
        plt.xlabel('Trials', size='large')
        plt.ylabel(key, size='large')
        plt.figtext(0.12, 0.02, "SimId=%d, Exp=%s" % (simId, expName),
                    color='black', weight='roman', size='x-small')

        if save:
            filename = makePlotPath("%s-s%02d-%s-%s" % (expName, simId, paramName, key), simId)
            _logger.debug("Saving convergence plot to %s" % filename)
            plt.savefig(filename)

        if show:
            plt.show()

    fig = plt.gcf()
    plt.close(fig)


# Could use series.describe() but I like this format better
def printStats(series):
    name   = series.name
    count  = series.count()
    mean   = series.mean()
    median = series.median()
    stdev  = series.std()
    skew   = series.skew()
    minv   = series.min()
    maxv   = series.max()
    ciLow  = series.quantile(0.025)
    ciHigh = series.quantile(0.975)

    print '''
%s:
     count: %d
      mean: %.2f
    median: %.2f
     stdev: %.2f
      skew: %.2f
       min: %.2f
       max: %.2f
    95%% CI: [%.2f, %.2f]''' % (name, count, mean, median, stdev, skew, minv, maxv, ciLow, ciHigh)


def normalize(series):
    'Normalize a series by dividing each element by the sum'
    total = series.sum()
    return series / total


def spearmanCorrelation(inputs, results):
    '''
    Compute Spearman ranked correlation and normalized Spearman ranked
    correlation between values in a DataFrame of inputs and a Series of
    results. Returns a Series with the spearman rank correlation values.
    '''
    corrList = [results.corr(inputs[col], method='spearman') for col in inputs.columns]
    spearman = pd.Series(data=corrList, index=inputs.columns, name='spearman')

    return spearman


def plotSensitivityResults(varName, data, filename=None, extra=None, printIt=True):
    '''
    Prints results and generates a tornado plot with normalized squares of Spearman
    rank correlations between an output variable and all input variables.
    '''
    # Sort descending by absolute value (normalized are all positive from squaring)
    data.sort(columns=['normalized'], ascending=False, inplace=True)

    if printIt:
        print "INDIVIDUAL PARAMETERS (%s)" % varName
        print "---------------------"
        print data.to_string(columns=['spearman', 'value'], float_format="{:4.2f}".format)

    title = 'Sensitivity of %s' % varName
    plotTornado(data, title=title, show=False, filename=filename, extra=extra)


def plotGroupSensitivityResults(varName, data, filename=None, extra=None, printIt=True):
    '''
    Sum the normalized contribution to variance for subscripted parameters,
    along with contribution from unsubscripted ones. For example, we sum
    the contributions for "ETA[1,5]" and "ETA[1,6]" into "ETA".
    '''
    totals = pd.Series(name='totals')

    for idx, row in data.iterrows():
        # Lop off bracketed indices at first '['.
        # e.g., from "ETA[1,5]", we'll extract "ETA"
        pos = idx.find('[')
        paramName = idx if pos < 0 else idx[0:pos]

        # Sum the values; initialize on demand
        try:
            totals[paramName] += row.value
        except KeyError:
            totals[paramName]  = row.value

    df = pd.DataFrame(totals)

    negatives = (totals < 0)
    df['sign'] = 1
    df.ix[negatives, 'sign'] = -1

    df['absval']    = totals * df['sign']
    df['normalize'] = normalize(df['absval'])
    df['value']     = df['normalize'] * df['sign']

    # Sort by absolute value
    df.sort(columns=['absval'], ascending=False, inplace=True)

    session  = getSession()
    result   = session.query(Input.paramName, Input.description).distinct().all()
    resultDF = pd.DataFrame(result)
    resultDF.fillna('')
    df['description'] = resultDF[0]

    if printIt:
        print ""
        print "PARAMETER GROUPS (%s)" % varName
        print "----------------"
        print df.to_string(columns=['value', 'description'], float_format="{:4.2f}".format)

    title = 'Sensitivity of %s' % varName
    plotTornado(df, title=title, figsize=None, show=False, filename=filename, extra=extra)


def plotInputDistributions(simId, inputDF):
    '''Plot the input values individually to test that the distributions are as expected'''

    cfg = getConfigManager()
    showHist  = cfg.getParamAsBoolean('Core.PlotShowHistogram')
    showKDE   = cfg.getParamAsBoolean('Core.PlotShowKDE')
    showShade = cfg.getParamAsBoolean('Core.PlotShowShading')

    for heading, series in inputDF.iteritems():
        plotHistogram(series, showCI=False,
                      xlabel='Parameter value', ylabel='Probability density',
                      title='Distribution for values of %s' % heading,
                      color=None, hist=showHist, kde=showKDE, shade=showShade,
                      show=False, filename=makePlotPath(heading, simId))


def plotOutputDistribution(simId, expName, resultSeries, resultName, xlabel, trials):
    filename = makePlotPath('%s-s%02d-%s-%d-trials' % (expName, simId, resultName, resultSeries.count()), simId)

    cfg = getConfigManager()
    showHist  = cfg.getParamAsBoolean('Core.PlotShowHistogram')
    showKDE   = cfg.getParamAsBoolean('Core.PlotShowKDE')
    showShade = cfg.getParamAsBoolean('Core.PlotShowShading')

    numValues = resultSeries.count()

    plotHistogram(resultSeries, xlabel=xlabel, ylabel='Probability density',
                  title='Frequency distribution for %s' % resultName,
                  extra='SimId=%d, Exp=%s, Trials=%d/%d' % (simId, expName, numValues, trials),
                  color=None, hist=showHist, kde=showKDE, shade=showShade,
                  showCI=True, showMean=True, showMedian=True, show=False, filename=filename)


def readParameterValues(simId, trials):
    def makeKey(paramName, row, col):
        return "%s[%d][%d]" % (paramName, row, col) if row or col else paramName

    db = getDatabase()

    paramTuples = db.getParameters()        # Returns paramName, row, col
    paramNames  = map(lambda tup: makeKey(*tup), paramTuples)   # names like "foo[1][14]"
    inputDF     = pd.DataFrame(index=xrange(trials), columns=paramNames, dtype=float)
    _logger.debug("Found %d distinct parameter names" % len(paramNames))

    cfg = getConfigManager()
    programs = cfg.getUserPrograms()

    for program in programs:    # TBD: deal with multiple programs
        _logger.info('Reading parameter values for %s' % program)
        paramValues = db.getParameterValues(simId, program, asDataFrame=False)
        numParams = len(paramValues) # paramValues.shape[0]
        _logger.info('Read %d parameter values' % numParams)

        for row, col, value, trialNum, pname in paramValues:
            key = makeKey(pname, row, col)
            inputDF[key][trialNum] = value

    return inputDF


def exportData(inputs, resultName, values, exportFile):
    # Add result to DF for easy export
    df = inputs.copy()
    df[resultName] = values

    _logger.debug("Exporting data to '%s'", exportFile)
    df.to_csv(exportFile, sep='\t')

def exportResults(simId, resultList, expList, exportFile, sep=','):
    db = getDatabase()
    df = None

    for expName in expList:
        for resultName in resultList:
            # resultDf has 'trialNum' as index, 'value' holds float value
            resultDf = db.getOutValues(simId, expName, resultName)
            if resultDf is None:
                raise CoreMcsUserError('No results were found for sim %d, experiment %s, result %s' % (simId, expname, resultName))

            # Add columns needed for boxplots
            resultDf['expName'] = expName
            resultDf['resultName'] = resultName

            resultDf.rename(columns = {resultName:'value'}, inplace=True)

            if df is None:
                df = resultDf
            else:
                df = pd.concat([df, resultDf])

    _logger.debug("Exporting results to '%s'", exportFile)
    df.to_csv(exportFile, sep=sep)

#
# Based on ema_workbench/core/utils.py:save_results()
#
def saveForEMA(simId, expNames, resultNames, inputDF, filename):
    '''
    Save simulation results to the specified tar.gz file. The results are
    stored as csv files. There is an x.csv, and a csv for each outcome. In
    addition, there is a metadata csv which contains the datatype information
    for each of the columns in the x array. Unlike the version of this function
    in the EMA Workbench, this version collects data from the SQL database to
    generate a file in the required format.

    Parameters
    ----------
    simId : int
              the id of the simulation
    expNames : list of str
            the names of the experiments to save results for
    resultNames : list of str
            the names of model outputs to save
    inputDF: pandas.DataFrame
            all model input values, each row holding values for 1 trial
    filename : str
                the path of the file

    Raises
    ------
    IOError if file not found

    '''
    from io import BytesIO
    import tarfile
    import time

    def add_file(tgzfile, string_to_add, filename):
        tarinfo = tarfile.TarInfo(filename)
        tarinfo.size = len(string_to_add)
        tarinfo.mode = 0644
        tarinfo.mtime = time.time()
        tgzfile.addfile(tarinfo, BytesIO(string_to_add.encode('UTF-8')))

    db = getDatabase()

    # InValue.row, InValue.col, InValue.value, Trial.trialNum, Input.paramName
    rows = inputDF.shape[0]

    with tarfile.open(filename, 'w:gz') as z:
        # Write the input values to the zipfile
        expData = inputDF.to_csv(None, sep=',', index=False) # index_label='trialNum'
        add_file(z, expData, 'experiments.csv')

        # Write experiment metadata
        dtypes = inputDF.dtypes
        # list(dtypes.iteritems()) produces results like:
        # [('A', dtype('int64')), ('B', dtype('int64')), ('C', dtype('float64'))] and
        # map(lambda dt: (dt[0], dt[1].descr), dtypes.iteritems()) produces:
        # [('A', [('', '<i8')]), ('B', [('', '<i8')]), ('C', [('', '<f8')])]
        # So, map(lambda dt: (dt[0], dt[1].descr[0][1]), dtypes.iteritems()) produces:
        # [('A', '<i8'), ('B', '<i8'), ('C', '<f8')]
        tuples = map(lambda dt: (dt[0], dt[1].descr[0][1]), dtypes.iteritems())
        strings = ["{},{}".format(name, dtype) for name, dtype in tuples]
        fileText = "\n".join(strings) + '\n'
        add_file(z, fileText, 'experiments metadata.csv')

        # Write outcome metadata    # TBD: deal with timeseries

        # outcome_meta = ["{},{}".format(outcome, ','.join(outcomes[outcome].shape))
        #                 for outcome in resultNames]
        strings = ["{},{}".format(resultName, rows) for resultName in resultNames]
        fileText = "\n".join(strings) + '\n'
        add_file(z, fileText, "outcomes metadata.csv")

        # Write outcomes
        for expName in expNames:
            for resultName in resultNames:
                outValueDF = db.getOutValues(simId, expName, resultName) # cols are trialNum and value; might need to do outValueDF[resultName].to_csv
                allTrialsDF = pd.DataFrame(index=xrange(rows))           # ensure that all trials are represented (with NA if need be)
                allTrialsDF[resultName] = outValueDF[resultName]
                fileText = allTrialsDF.to_csv(None, header=False, index=False)
                fname = "{}-{}.csv".format(resultName, expName)
                add_file(z, fileText, fname)

    print "Results saved successfully to {}".format(filename)

def analyzeSimulation(args):
    '''
    Analyze a simulation by reading parameters and results from the database.
    '''
    simId       = args.simId
    expNames    = args.expName
    plotHist    = args.plot
    stats       = args.stats
    importance  = args.importance
    groups      = args.groups
    plotInputs  = args.plotInputs
    convergence = args.convergence
    resultName  = args.resultName
    limit       = args.limit
    xlabel      = args.xlabel
    exportFile  = args.exportFile
    resultFile  = args.resultFile
    exportEMA   = args.exportEMA

    inputDF = None

    db = getDatabase()
    trials = db.getTrialCount(simId) if limit < 0 else limit
    if not trials:
        raise CoreMcsUserError('No trials were found for simId %d' % simId)

    expList = expNames and expNames.split(',')
    if not (expList[0] and resultName):
        raise CoreMcsUserError("expName and resultName must be specified")

    if resultFile:
        resultList = resultName.split(',')
        exportResults(simId, resultList, expList, resultFile)
        return

    if exportEMA:
        inputDF = readParameterValues(simId, trials)
        resultList = resultName.split(',')
        saveForEMA(simId, expList, resultList, inputDF, exportEMA)
        return

    # inputs are shared across experiments, so gather these before looping over experiments
    if plotInputs or exportFile or importance or groups:
        inputDF = readParameterValues(simId, trials)
        inputRows, inputCols = inputDF.shape
        _logger.info("Each trial has %d parameters", inputCols)

    if plotInputs:
        plotInputDistributions(simId, inputDF)

    if not (exportFile or importance or groups or plotHist or convergence or stats):
        return

    for expName in expList:
        resultDF = db.getOutValues(simId, expName, resultName, limit=limit)
        if resultDF is None:
            raise CoreMcsSystemError('analyzeSimulation: No results for simId=%d, expName=%s, resultName=%s' % (simId, expName, resultName))

        resultSeries = resultDF[resultName]
        numResults = resultSeries.count()

        if plotHist:
            plotOutputDistribution(simId, expName, resultSeries, resultName, xlabel, trials)

        if stats:
            printStats(resultSeries)

        if convergence:
            plotConvergence(simId, expName, resultName, resultSeries, show=False, save=True)

        if (importance or groups or exportFile) and (numResults != trials or numResults != inputRows):
            _logger.info("SimID %d has %d trials, %d input rows, and %d results", simId, trials, inputRows, numResults)

        if exportFile:
            exportData(inputDF, resultName, resultSeries, exportFile)

        if importance or groups:
            spearman = spearmanCorrelation(inputDF, resultSeries)

            data = pd.DataFrame(spearman)
            data['normalized'] = normalize(spearman ** 2)
            data['sign'] = 1
            negatives = (data.spearman < 0)
            data.ix[negatives, 'sign'] = -1
            data['value'] = data.normalized * data.sign     # normalized squares with signs restored

            if importance:
                plotSensitivityResults(resultName, data,
                                        filename=makePlotPath('%s-s%02d-%s-ind' % (expName, simId, resultName), simId),
                                        extra='SimId=%d, Exp=%s, Trials=%d/%d' % (simId, expName, numResults, trials))

            if groups:
                plotGroupSensitivityResults(resultName, data,
                                             filename=makePlotPath('%s-s%02d-%s-grp' % (expName, simId, resultName), simId),
                                             extra='SimId=%d, Exp=%s, Trials=%d/%d' % (simId, expName, numResults, trials))
