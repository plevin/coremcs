'''
Created on Sep 22, 2013

CoreConfiguration file manipulation routines.

@author: Richard Plevin

Copyright (c) 2014. The Regents of the University of California (Regents).
See the file COPYRIGHT.txt for details.
'''

import sys
import os
from ConfigParser import SafeConfigParser, NoSectionError, NoOptionError
from .error import CoreMcsSystemError, CoreMcsUserError

DEFAULT_SECTION   = 'DEFAULT'
APP_DIR_VAR       = 'Core.AppDir'           # Top-level application directory
RUN_DIR_VAR       = 'Core.RunDir'
DEFAULT_APP_VAR   = 'Core.DefaultApp'
PROGRAMS          = 'Core.Programs'
APP_NAME_OPTION   = 'Core.AppName'

USER_CONFIG_PATH  = os.path.join(os.path.expanduser('~'), '.config', 'mcs', 'user.cfg')
PKG_CFG_FILE      = os.path.join('cfg', 'defaults.cfg')
PKG_TEMPLATE_DIR  = 'template'

Verbose = False     # logging depends on Config system, so we don't use it here


class ConfigFileError(CoreMcsUserError):
    '''Error related to user configuration file.'''
    pass


_ConfigMgr = None   # the one instance of a CoreConfiguration that is created

def getConfigManager(cls=None):
    '''
    Class argument (cls) is used only on first call. Thereafter, the
    instance of the class used in the first call is returned.
    '''
    global _ConfigMgr

    if not _ConfigMgr:
        if not cls:
            raise CoreMcsSystemError("First call to getConfigManager is missing a 'class' argument")

        _ConfigMgr = cls()                 # reads package defaults, following up class hierarchy
        _ConfigMgr.readUserConfigFile()    # Read user config file after all the package configs

    return _ConfigMgr


def ensureSection(parser, section):
    '''
    Add a section if needed.
    '''
    if not parser.has_section(section):
        parser.add_section(section)


class CoreConfiguration(object):

    def __init__(self):
        '''
        Subclasses must call super.__init__() first, then call their own
        self.readPackageDefaults(__file__) to pick up local package defaults.
        '''
        self.appName = None     # cache current appName, which may differ from default in config file

        env = os.environ.copy()

        # Get rid of anything that will foul up the config system, i.e., anything with '%' in the value
        for key, val in env.items():
            if r'%' in key or r'%' in val:
                del env[key]

        # seed the defaults with all defined environment vars
        self.dfltParser = SafeConfigParser(env)

        # allocate separate parser for the user's customization file (~/.mcs/user.cfg)
        self.userParser = SafeConfigParser()

        self.templateDir = None

        # Read defaults associated with the coremcs "package". Subclasses __init__ methods must
        # call readPackageDefaults() after calling super(SUBCLASS, self).__init__() to ensure that
        # the files are read from the innermost level to the outermost so that shadowing behaves
        # as expected.
        self.readPackageDefaults(__file__)


    def mergeParsers(self):
        '''
        Copy user parser onto dfltParser, which is used for lookups. This copy occurs
        any time the user changes a value, which always applies to userParser (and is
        written out to ~/.config/mcs/user.cfg)
        '''
        dfltParser = self.dfltParser
        userParser = self.userParser

        # copy user's global defaults
        defaults = userParser.defaults()
        for key, value in defaults.iteritems():
            dfltParser.set(DEFAULT_SECTION, key, value)

        # copy any other existing sections
        for section in userParser.sections():
            ensureSection(dfltParser, section)
            for key, value in userParser.items(section, raw=True):
                dfltParser.set(section, key, value)


    @classmethod
    def configClassInfo(cls):
        '''
        Return pathname and classname of the current config class so the 'queue'
        subcmd can pass this information to Runner.py, which can then instantiate
        the config class associated with the package.
        '''
        modname   = getattr(cls, '__module__')
        module    = sys.modules[modname]
        pathname  = os.path.abspath(getattr(module, '__file__'))
        classname = getattr(cls, '__name__')
        return pathname, classname



    def writeUserConfig(self):
        # Try to create a backup first, but ignore error if it fails
        try:
            os.rename(USER_CONFIG_PATH, USER_CONFIG_PATH+'~')
        except OSError:
            pass

        (folder, filename) = os.path.split(USER_CONFIG_PATH)
        if not os.path.exists(folder):
            os.mkdir(folder)

        with open(USER_CONFIG_PATH, 'w+') as f:
            f.write('#\n# This is a generated file. Manual edits will be lost!\n# Use "coremcs config" to modify parameters.\n#\n')
            self.userParser.write(f)


    def readPackageDefaults(self, moduleFile):
        '''
        Load configuration file cfg/defaults.cfg from package directory
        containing the CoreConfigMgr class or subclass.
        NB: Must be called after calling super's __init__.
        '''
        if Verbose:
            print 'moduleFile', moduleFile

        packageRoot  = os.path.dirname(os.path.realpath(moduleFile))
        if Verbose:
            print "Package root is ", packageRoot

        defaultsFile = os.path.join(packageRoot, PKG_CFG_FILE)
        if Verbose:
            print "Reading", defaultsFile
        self.dfltParser.readfp(open(defaultsFile))

        # Compute the absolute path to our package's template directory.
        # Last package read from sets the final templateDir
        self.templateDir = os.path.join(packageRoot, PKG_TEMPLATE_DIR)


    def readUserConfigFile(self):
        'The user config file is read after the hierarchy of package config files are read'
        path = USER_CONFIG_PATH
        if os.path.exists(path):
            if Verbose:
                print "Reading", path
            self.userParser.read(path)  # keep separate copy for writing back to same file
            self.mergeParsers()

            if not self.appName:
                appName = self.userParser.get(DEFAULT_SECTION, APP_NAME_OPTION)
                if appName:
                    self.setDefaultAppName(appName)
        else:
            print "WARNING: User config file %s was not found" % path


    def getTemplateDir(self):
        return self.templateDir


    def setAppVariable(self, appName, variable, value):
        ''''
        Save a config variable in the section for the named app.
        '''
        # If a path was given, add it to the file in a section named for the model
        ensureSection(self.userParser, appName)
        self.userParser.set(appName, variable, value)
        self.writeUserConfig()
        self.mergeParsers()

    def setAppDir(self, appName, pathname):
        ''''
        Save the app-to-path mapping in the app's config file by setting the
        AppDir variable in a section named for the app to the given path.
        '''
        self.setAppVariable(appName, APP_DIR_VAR, pathname)


    def setRunDir(self, appName, pathname):
        ''''
        Save the app-to-path mapping in the app's config file by setting the
        AppDir variable in a section named for the app to the given path.
        '''
        self.setAppVariable(appName, RUN_DIR_VAR, pathname)


    def setDefaultAppName(self, appName):
        '''
        Set the current app name and, if setDefault is True, set the
        parsers accordingly.
        '''
        if Verbose:
            print "Setting appName to %s" % appName
        self.appName = appName

        ensureSection(self.userParser, appName)
        self.setParam(APP_NAME_OPTION, appName, section=DEFAULT_SECTION)

    def getDefaultAppName(self):
        return self.getParam(APP_NAME_OPTION, DEFAULT_SECTION)

    def setAppName(self, appName, setDefault=False):
        if setDefault:
            self.setDefaultAppName(appName)     # sets self.appName and saves to user config file
        else:
            self.appName = appName              # sets self.appName for run-time use, without saving to file

    def getAppName(self):
        if not self.appName:
            self.appName = self.getDefaultAppName()

        return self.appName

    def setParam(self, option, value, section=None):
        if section is None:
            section = self.getAppName()

        try:
            self.userParser.set(section, option, str(value))

        except NoSectionError:
            print "setParam(%s,%s,%s)" % (option, value, section)
            print 'Section "%s" not found in config files' % section
            raise

        self.writeUserConfig()
        self.mergeParsers()


    def getParam(self, option, section=None, default=None):
        '''
        Look for a parameter in the given section, or DEFAULT, in dlftParser,
        which holds the values read from the Packages. If not found anywhere,
        return the default value passed in by the caller.
        '''
        if section is None:
            section = self.getAppName()

        # All lookups are done on the lookupParser since it has the combined values
        dlftParser = self.dfltParser

        try:
            return dlftParser.get(section, option)

        except NoSectionError:
            return dlftParser.get(DEFAULT_SECTION, option)

            #print "getParam(%s,%s,%s)" % (option, section, default)
            #print 'Section "%s" not found in config files' % section

        except NoOptionError:
            if default is not None:
                # if a default value is passed in, quietly return it
                print 'Option "%s" not found in section "%s" in config files' % (option, section)

        return default


    def listSection(self, section):
        if self.dfltParser.has_section(section):
            print "[%s]" % section
            for var, val in sorted(self.dfltParser.items(section)):
                print "%20s = %s" % (var, val)


    def getParamAsInt(self, option, section=None, default=None):
        value = self.getParam(option, section=section, default=default)
        return int(value)


    def getParamAsFloat(self, option, section=None, default=None):
        value = self.getParam(option, section=section, default=default)
        return float(value)


    def getParamAsBoolean(self, option, section=None, default=None):
        value = self.getParam(option, section=section, default=default)
        return value and str(value).lower() in ('true', 'yes', 'on', '1') # added str() to avoid spurious error message


    def getUserPrograms(self):
        programs = self.getParam(PROGRAMS, default='')
        programs = programs.strip()
        return programs.split(' ') if len(programs) > 0 else None
