'''
Created on Jul 10, 2012

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''


class CorrelationDef(object):
    '''
    Simple class to hold correlation information read from distribution file.
    '''
    def __init__(self, name1, arg2, arg3=None):
        #
        # A corrSpec of 3 elements indicates a correlation between the
        # two named parameters, each of which must correspond to an item
        # in self.rvList, or for matrix RVs of the same dimensions,
        # pairwise correlate items in the same position in each matrix.
        #
        # A corrSpec of 2 elements indicates an intra-matrix correlation,
        # i.e., among elements of matrix 'name'.
        #
        self.name1 = name1
        self.name2 = arg3 and arg2
        self.coef = float(arg3 or arg2)

    def __str__(self):
        classname = type(self).__name__
        name2str = "" if self.name2 is None else " %s" % self.name2
        return "<%s %s%s %.2f>" % (classname, self.name1, name2str, self.coef)


class CorrelationPair(object):
    '''
    Holds a definition of a specific pairwise correlation.
    '''
    def __init__(self, name1, name2, coef):
        self.name1 = name1
        self.name2 = name2
        self.coef = coef

    def __str__(self):
        classname = type(self).__name__
        return "<%s %s %s %.2f>" % (classname, self.name1, self.name2, self.coef)
