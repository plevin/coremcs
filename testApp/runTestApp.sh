#!/bin/bash
# Author: Richard Plevin (rich@plevin.com)
#
# Copyright (c) 2012-2015. The Regents of the University of California (Regents)
# and Richard Plevin. See the file COPYRIGHT.txt for details.

testDir=$HOME/mcs-test
runRoot=$testDir/run
appRoot=$testDir/app

appName=testApp
mcs="./testmcs -A ${appName}"

# Remove any prior installed version of the testapp
echo 'Removing old test files'
rm -rf $runRoot/$appName $appRoot/$appName

echo ""
echo "Creating the application '$appName'"
cmd="$mcs new -r $runRoot -a $appRoot"
status=$?
echo $cmd
if ! $cmd; then exit 1; fi

echo ""
echo "Generating a simulation with 3 trials"
cmd="$mcs gensim -t 3"
status=$?
echo $cmd
if ! $cmd; then exit 1; fi

echo ""
echo "Run 3 trials of simulation 1, for experiment testexp"
cmd="$mcs queue -E -s1 -t0-2 -e testexp"
echo $cmd
$cmd

echo ""
echo "When the program completes (the job queue no longer shows the job), run this"
echo "command to view the result log:"
echo ""
echo "  cat $runRoot/$appName/sims/s001/log/testexp-s01-0.err"
