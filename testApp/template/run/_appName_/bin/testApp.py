#!/usr/bin/env python
'''
Simple app to test that coremcs system is setup correctly.
This program is run on each compute node.

@author: Rich Plevin

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import sys
import os
import time
import argparse

from coremcs import log
from coremcs.Configuration import getConfigManager
from coremcs.util import getContext
from coremcs.Database import setOutValue
from coremcs.testApp.Package import TestPackage, TestConfiguration

_VERSION = "testApp v1"

SLEEP_SECONDS = 1

def parseArgs(argv):
    parser = argparse.ArgumentParser(description='''testapp sleeps the number of seconds
    indicated by the -s argument and then exits with the exit code given by the -e flag.''')

    parser.add_argument('-a', dest='abort', action='store_true',
                        help='Sleep the given number of seconds, and then abort.')

    parser.add_argument('-s', dest='seconds', type=int, default=SLEEP_SECONDS,
                        help='The number of seconds to sleep (default is %d)' % SLEEP_SECONDS)

    parser.add_argument('-e', dest='exitCode', type=int, default=0,
                        help='The status code to exit with (default is 0)')

    args = parser.parse_args(argv)
    return args


def runner(argv):
    '''
    By defining a "runner" function, the coremcs Runner.py will call this directly, not
    requiring a separate process. The parameter argv must be formed like sys.argv: the
    first element should be the program name, followed by any args.
    The program sleeps the number of seconds in argument to the -s option (default=3),
    writes some log messages, computes a result and saves it in the database, and exits
    with the exit code given with the -e option (default=0).
    '''
    logger = log.getLogger(__name__)

    # Establish the config manager so that local defaults.cfg is read.
    # If this app defines any additional config parameters, it must
    # subclass CoreConfiguration (or it's subclass) and provide it here.
    cfg = getConfigManager(cls=TestConfiguration)
    log.configure(cfg)

    # If defined in the config file, use it as the default. Cmd line can override.
    global SLEEP_SECONDS
    SLEEP_SECONDS = cfg.getParamAsInt('Test.SleepSeconds', default=SLEEP_SECONDS)

    exe  = os.path.basename(argv[0])
    args = parseArgs(argv[1:])

    program = cfg.getParam('Core.Programs') # we assume there's only one specified here...

    # Read this trial's parameter file
    paramSetClass = TestPackage.getParameterSetClass(program)
    paramClass = paramSetClass.parameterClass()

    filename = "../%s.param" % program
    params = paramClass.readFile(filename)
    logger.debug("Read %d parameters from %s" % (len(params), filename))

    # Multiply together the parameters to create a simple model output
    volume = params['height'].data * params['width'].data * params['depth'].data

    # Get our runId from environment "context"
    c = getContext()

    # Print the result to the log file.
    logger.info("SimId=%d TrialNum=%d ExpName=%s : Volume=%f", c.simId, c.trialNum, c.expName, volume)

    # Save the result 'volume' to outvalue table in the SQL database
    setOutValue('volume', volume, program=program)

    logger.info('%s: sleeping %d seconds', exe, args.seconds)
    time.sleep(args.seconds)

    if args.abort:
        raise Exception('testApp aborted by request')

    logger.info('%s: runner returning exit status %d', exe, args.exitCode)
    return args.exitCode

if __name__ == '__main__':
    # Runs the model from the command line
    status = runner(sys.argv)
    sys.exit(status)
