'''
Created on 11/8/14

@author : rjp

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import geopandas as gpd
import matplotlib.pyplot as plt


# TODO
# - Create a dataframe of values to plot
# - Create a legend (only for categorical plots currently?)
# - Create shapefiles with regions merged (just AEZs) and AEZs merged (just regions) to eliminate extra lines
# - Experiment with styles and different color sets (e.g., 3 bands for AEZs R, G, B, with different shades

def main():
    DPI = 400

    # countryFile = '/Users/rjp/Projects/Data/GIS/NaturalEarth/all_10m_20/ne_10m_admin_0_countries/ne_10m_admin_0_countries.shp'
    # shapeFile = '/Users/rjp/Projects/Data/GIS/AEZ/GAEZ-GIS/Vector/ctrygaez.shp'
    # df = gpd.GeoDataFrame.from_file(shapeFile)
    #
    # df.plot(categorical=True)
    # plt.savefig('/tmp/gtapRegAEZ.pdf', dpi=DPI)

    # Documentation:
    # savefig(fname, dpi=None, facecolor='w', edgecolor='w',
    #         orientation='portrait', papertype=None, format=None,
    #         transparent=False, bbox_inches=None, pad_inches=0.1,
    #         frameon=None)

    gcamShapeFile = '/Users/rjp/Projects/Data/GIS/GCAM/GCAM32_Modified/gcam32Reg18AEZ.shp'
    gcamDF = gpd.GeoDataFrame.from_file(gcamShapeFile)
    print "Columns:", gcamDF.columns
    print "plotting..."
    figsize = (12, 6)
    plt.figure(facecolor='white', figsize=figsize)
    ax = plt.gca()
    gcamDF.plot(column='REGION', axes=ax) # categorical=True, legend=True
    filename = '/tmp/gcamRegions.png'
    print "saving %s..." % filename
    plt.savefig(filename, dpi=DPI)

    print "plotting..."
    plt.figure(facecolor='white', figsize=figsize)
    ax = plt.gca()
    gcamDF.plot(column='AEZ', axes=ax, categorical=True, legend=True)
    filename = '/tmp/gcamAEZs.png'
    print "saving %s..." % filename
    plt.savefig(filename, dpi=DPI)

main()
