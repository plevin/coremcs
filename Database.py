'''
Created on Mar 9, 2012

@author: Richard Plevin
@author: Sam Fendell
@author: Ryan Jones

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import sys
from datetime import datetime
from collections import Iterable
from contextlib import contextmanager

from sqlalchemy import create_engine, Column, Integer, String, Float, Boolean, \
    text, ForeignKey, DateTime, MetaData, UniqueConstraint, Index, event
from sqlalchemy.engine import Engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declared_attr, declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.pool import NullPool

from . import log
from .error import CoreMcsUserError, CoreMcsSystemError
from . import util as U
from .Configuration import getConfigManager

_logger = log.getLogger(__name__)

#
# Object-relational mapping; defines database tables
#
ORMBase = declarative_base()

class CoreMCSMixin(object):

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

class Application(CoreMCSMixin, ORMBase):
    appId   = Column(Integer, primary_key=True)
    appName = Column(String)

class Attribute(CoreMCSMixin, ORMBase):
    attrId      = Column(Integer, primary_key=True)
    attrName    = Column(String)
    attrType    = Column(String)    # should be "number" or "text" (not used currently)
    description = Column(String, nullable=True)

class AttrVal(CoreMCSMixin, ORMBase):
    attrId   = Column(Integer, primary_key=True)
    runId    = Column(Integer, ForeignKey('run.runId', ondelete="CASCADE"), primary_key=True)
    numValue = Column(Float)
    strValue = Column(String)

class Code(CoreMCSMixin, ORMBase):
    #codeId      = Column(Integer, primary_key=True, autoincrement=False)
    codeName    = Column(String(15), primary_key=True)
    description = Column(String(256))

class Experiment(CoreMCSMixin, ORMBase):
    expId       = Column(Integer, primary_key=True)
    expName     = Column(String(30))
    description = Column(String(256))
    __table_args__ = (Index("experiment_index1", "expName", unique=True),)

class Input(CoreMCSMixin, ORMBase):
    __table_args__ = (UniqueConstraint('programId', 'paramName'),)

    inputId     = Column(Integer, primary_key=True)
    programId   = Column(Integer, ForeignKey('program.programId', ondelete="CASCADE"))
    paramName   = Column(String)
    description = Column(String, nullable=True)

# TBD: add a variableNumber column to be used in cases like GCAM that aren't matrix oriented?
class InValue(CoreMCSMixin, ORMBase):
    inputId  = Column(Integer, ForeignKey('input.inputId', ondelete="CASCADE"), primary_key=True)
    simId    = Column(Integer, ForeignKey('sim.simId', ondelete="CASCADE"))
    trialNum = Column(Integer, primary_key=True)
    row      = Column(Integer, primary_key=True)
    col      = Column(Integer, primary_key=True)
    value    = Column(Float)
    __table_args__ = (Index("invalue_index1", "inputId", unique=False),)

class Output(CoreMCSMixin, ORMBase):
    outputId    = Column(Integer, primary_key=True)
    programId   = Column(Integer, ForeignKey('program.programId', ondelete="CASCADE"))
    name        = Column(String)
    timeseries  = Column(Boolean, default=False)
    description = Column(String, nullable=True)

class OutValue(CoreMCSMixin, ORMBase):
    outputId = Column(Integer, ForeignKey('output.outputId', ondelete="CASCADE"), primary_key=True)
    runId    = Column(Integer, ForeignKey('run.runId', ondelete="CASCADE"), primary_key=True)
    value    = Column(Float)

class Program(CoreMCSMixin, ORMBase):
    programId   = Column(Integer, primary_key=True)
    name        = Column(String)
    description = Column(String, nullable=True)

class Run(CoreMCSMixin, ORMBase):
    runId     = Column(Integer, primary_key=True)
    simId     = Column(Integer, ForeignKey('sim.simId', ondelete="CASCADE"))
    expId     = Column(Integer, ForeignKey('experiment.expId', ondelete="CASCADE"))
    trialNum  = Column(Integer)
    jobNum    = Column(String)
    queueTime = Column(DateTime, default=datetime.now)
    startTime = Column(DateTime, nullable=True)
    endTime   = Column(DateTime, nullable=True)
    duration  = Column(Integer,  nullable=True)
    status    = Column(String,   nullable=True)
    __table_args__ = (Index("run_index1", "simId", "trialNum", "expId", unique=True),)

class Sim(CoreMCSMixin, ORMBase):
    simId       = Column(Integer, primary_key=True)
    trials      = Column(Integer)
    description = Column(String, nullable=True)
    stamp       = Column(DateTime, default=datetime.now, onupdate=datetime.now)


def usingSqlite():
    '''
    Return True if the DbURL indicates that we're using Sqlite, else return False
    '''
    cfg = getConfigManager()
    url = cfg.getParam('Core.DbURL')
    return url.lower().startswith('sqlite')

def usingPostgres():
    '''
    Return True if the DbURL indicates that we're using Postgres, else return False
    '''
    cfg = getConfigManager()
    url = cfg.getParam('Core.DbURL')
    return url.lower().startswith('postgres')


@event.listens_for(Engine, "connect")
def sqlite_FK_pragma(dbapi_connection, connection_record):
    '''Turn on foreign key support in sqlite'''
    if usingSqlite():
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON")
        cursor.close()

# Constants to avoid misspelling errors
DFLT_PROGRAM  = 'default'

# Status strings for Run table
RUN_QUEUED    = 'queued'
RUN_RUNNING   = 'running'
RUN_SUCCEEDED = 'succeeded'
RUN_FAILED    = 'failed'
RUN_KILLED    = 'killed'
RUN_ABORTED   = 'aborted'
RUN_ALARMED   = 'alarmed'

def beforeSavingRun(_mapper, _connection, run):
    '''
    Before inserting/updating a Run instance, set numerical status and
    timestamps according to the status string.
    '''
    status = run.status

    if status == RUN_QUEUED:
        run.queueTime = datetime.now()
        run.startTime = None
        run.endTime   = None
        run.duration  = None
    elif status == RUN_RUNNING:
        run.startTime = datetime.now()
        run.endTime   = None
        run.duration  = None
    elif status in (RUN_SUCCEEDED, RUN_FAILED, RUN_KILLED, RUN_ABORTED, RUN_ALARMED):
        run.endTime   = datetime.now()
        delta         = run.endTime - run.startTime
        run.duration  = delta.seconds / 60


# Associate a listener function with Run, to execute before inserts and updates
event.listen(Run, 'before_insert', beforeSavingRun)
event.listen(Run, 'before_update', beforeSavingRun)


class CoreDatabase(object):

    def __init__(self):
        self.Session = sessionmaker()    # N.B. sessionmaker returns a class object
        self.url     = None
        self.engine  = None
        self.appId   = None

    # TBD: unused currently. Deprecated?
    @contextmanager
    def session_scope(self):
        """
        Provide a transactional scope around a series of operations.
        """
        session = self.Session()
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

    # create_engine(*args, **kwargs)
    # The string form of the URL is dialect+driver://user:password@host/dbname[?key=value..],
    # where dialect is a database name such as mysql, oracle, postgresql, etc., and driver
    # the name of a DBAPI, such as psycopg2, pyodbc, cx_oracle, etc.
    # psycopg2: http://www.stickpeople.com/projects/python/win-psycopg/
    #
    # postgres|mysql: engine = create_engine('postgresql://scott:tiger@localhost/mydatabase')
    #
    # sqlite:   engine = create_engine('sqlite:///foo.db') -- if relative pathname, or
    #           engine = create_engine('sqlite:////absolute/path/to/foo.db') if abs pathname.
    #
    # To add a user and database in postgres:
    #    createuser -P mcsuser      # -P => prompt for password
    #    createdb -O mcsuser mcs    # -O => make mcsuser owner of database mcs
    #
    def startDb(self, checkInit=True):
        '''
        Links session to the database file identified in the config file as 'dbfile'.
        This needs to be called before any database operations can occur. It is called
        in getDatabase() when a new database instance is created.
        '''
        cfg  = getConfigManager()
        url  = cfg.getParam('Core.DbURL')
        echo = cfg.getParamAsBoolean('Core.EchoSQL')

        _logger.info('Starting DB: %s' % url)

        self.createDatabase()

        connect_args = {'connect_timeout': 15} if usingPostgres() else {}

        # Use NullPool to avoid having hundreds of trials holding connections open
        self.engine = engine = create_engine(url, echo=echo, poolclass=NullPool, connect_args=connect_args)
        self.Session.configure(bind=engine)

        self.url = url

        if checkInit:
            # Load metadata from the existing database, not from the ORMBase,
            # to see if the "run" table exists. If not, initialize the DB.
            # We don't do this if calling from Runner.py, which requires that
            # the database be set up already.
            meta = MetaData(bind=engine, reflect=True)
            if 'run' not in meta.tables:
                self.initDb()


    def initDb(self, args=None):
        '''
        Initialize the database, including loading required inserts.
        '''
        _logger.info('Initializing DB: %s' % self.url)

        meta = ORMBase.metadata     # accesses declared tables
        meta.bind = self.engine
        meta.reflect()
        meta.drop_all()

        session = self.Session()
        meta.create_all()
        session.commit()

        if args and args.empty:
            return

        _logger.debug('Adding standard codes')
        # Add standard app status codes
        session.add(Code(codeName=RUN_QUEUED,    description='Trial queued'))
        session.add(Code(codeName=RUN_RUNNING,   description='Trial running'))
        session.add(Code(codeName=RUN_SUCCEEDED, description='Trial succeeded'))
        session.add(Code(codeName=RUN_FAILED,    description='Trial failed'))
        session.add(Code(codeName=RUN_ABORTED,   description='Runtime error'))
        session.add(Code(codeName=RUN_KILLED,    description='System timeout'))
        session.add(Code(codeName=RUN_ALARMED,   description='Runner timeout.'))

        #session.add(Program(name=DFLT_PROGRAM, description='Program name used when none is specified'))
        _logger.debug('Committing standard codes')
        session.commit()

        pkg = U.getRunningPackage()
        initialData = pkg.getInitialData()          # traverses package hierarchy, collecting initial data

        _logger.debug('Adding initial data')
        for key, value in initialData:
            # If no module is specified with the class name, it is
            # assumed to be in this module, otherwise everything
            # up to the last '.' is treated as the module name
            items     = key.rsplit('.', 1)
            modName   = items[0] if len(items) == 2 else __name__
            className = items[1] if len(items) == 2 else items[0]

            table = className.lower()
            if table in ORMBase.metadata.tables:
                module    = sys.modules[modName]
                dataClass = getattr(module, className)
                if not dataClass:
                    raise CoreMcsSystemError("Table class %s not found in module %s" % (className, modName))

                for row in value:
                    session.add(dataClass(**row))
                    _logger.debug('committing row')
                    session.commit()                # commit each row so each can refer to prior rows
            else:
                raise KeyError(table)

        pkg.createOutputs(self)


    def createDatabase(self):
        '''
        Ensure that the database directory (in the case of sqlite3) or the database is available.
        '''
        cfg  = getConfigManager()

        if usingSqlite():
            # Make sure required directory exists
            dbDir = cfg.getParam('Core.RunDbDir')
            U.mkdirs(dbDir)
            return

        if usingPostgres() and cfg.getParam('Core.Postgres.CreateDbExe'):
            import subprocess, shlex, re
            from coremcs.error import CoreMcsSystemError

            # Make sure required database exists
            dbName   = cfg.getAppName()
            createdb = cfg.getParam('Core.Postgres.CreateDbExe')
            argStr   = cfg.getParam('Core.Postgres.CreateDbArgs')
            command  = "%s %s" % (createdb, argStr)
            _logger.debug("Trying command: %s" % command)
            args = shlex.split(command)

            proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            if not proc:
                raise CoreMcsSystemError("Could not run: %s" % command)

            status = proc.wait()      # wait for process to complete
            if status == 0:
                _logger.debug("Created postgres database '%s'", dbName)
            else:
                output = list(proc.stdout)
                existsMsg = 'database "%s" already exists' % dbName
                dbExists  = any(map(lambda line: re.search(existsMsg, line), output))

                if not dbExists:
                    raise CoreMcsSystemError("%s failed: %s" % (command, dbExists))     # fail if unexpected error occurs

                _logger.debug("Postgres database '%s' already exists", dbName)

    def commitWithRetry(self, session):
        import sqlite3
        import random
        import time

        # We retry up to 20 times, with random delay of between 3 and 15 seconds
        tries = 0
        limit = 20
        maxSleep = 15.0

        done = False
        while not done:
            try:
                session.commit()
                done = True
            except sqlite3.OperationalError, e:
                if tries < limit:
                    delay = random.random() * maxSleep    # sleep for a random number of seconds up to maxSleep
                    _logger.debug('sqlite3 operational error: %s', e)
                    _logger.warn("Database locked (retry %d); sleeping %.1f sec" % (tries, delay))
                    time.sleep(delay)
                    tries += 1
                    continue
                raise CoreMcsSystemError("Failed to acquire database lock")

            except Exception as e:
                raise CoreMcsSystemError("commitWithRetry error: %s" % e)

    def execute(self, sql):
        'Execute the given SQL string'
        _logger.debug('Executing SQL: %s' % sql)
        session = self.Session()
        session.execute(sql)
        self.commitWithRetry(session)
        session.close()

    def dropAll(self):
        '''
        Drop all objects in the database, even those not defined in SqlAlchemy.
        '''
        session = self.Session()
        engine  = session.get_bind()

        meta = MetaData(bind=engine)
        meta.reflect()

        if usingPostgres():
            self.dropAllPostgres(meta)
        else:
            meta.drop_all()

        session.commit()
        session.close()

    def dropAllPostgres(self, meta):
        """
        Drop all postgres tables and sequences from schema 'public'
        """
        for type in ('table', 'sequence'):
            sql = "select {type}_name from information_schema.{type}s where {type}_schema='public'".format(type=type)
            names = [name for (name, ) in meta.execute(text(sql))]
            for name in names:
                try:
                    meta.execute(text("DROP %s %s CASCADE" % (type, name)))
                except SQLAlchemyError, e:
                    print e

    def setSqlEcho(self, value=True):
        session = self.Session()
        engine = session.get_bind()
        engine.echo = value
        session.close()

    def addColumns(self, tableClass, columns):
        '''
        Adds a new column or columns to an existing table, emitting an
        ALTER TABLE statement and updating the metadata.
        :param tableClass: the class defining the table to alter
        :param column: a Column instance describing the column to add
        :return: none
        '''
        if not isinstance(columns, Iterable):
            columns = [columns]

        session = self.Session()
        engine  = session.get_bind()
        table   = tableClass.__table__
        tableName  = table.description
        for column in columns:
            table.append_column(column)                     # add it to the metadata
            setattr(tableClass, column.name, column)        # add attribute to class, which maps it to the column
            columnName = column.name                        # column.compile(dialect=engine.dialect)
            columnType = column.type.compile(engine.dialect)
            engine.execute('ALTER TABLE %s ADD COLUMN "%s" %s' % (tableName, columnName, columnType))
        session.close()

    def createAttribute(self, attrName, attrType, description):
        '''
        Create a new attribute. Unclear if this is needed, or if this will be
        handled using the database customization module. Useful for testing.
        '''
        session = self.Session()
        attr = Attribute(attrName=attrName, attrType=attrType, description=description)
        session.add(attr)
        session.commit()
        attrId = attr.attrId
        session.close()
        return attrId

    def setAttrVal(self, runId, attrName, numValue=None, strValue=None, session=None):
        '''
        Set the given named attribute to the given numeric or string value. Overwrite a
        previous value for this runId and attribute, if found, otherwise create a new value
        record. If session is not provided, one is allocated, and the transaction is
        committed. If a session is provided, the caller is responsible for calling commit.
        This allows numerous attribute values to be set in one transaction.
        '''
        assert (not (numValue is None and strValue is None)), "setAttrValue called with neither a numValue or a strValue"

        sess = session or self.Session()
        attr = sess.query(Attribute).filter_by(attrName=attrName).scalar()
        if not attr:
            raise CoreMcsSystemError("setAttrValue: unknown attribute '%s'" % attrName)

        # If previous value is found, overwrite it; otherwise create a new one
        attrVal = sess.query(AttrVal).filter_by(runId=runId, attrId=attr.attrId).first()

        if attrVal is None:
            attrVal = AttrVal(runId=runId, attrId=attr.attrId)
            sess.add(attrVal)

        # Allow setting of either or both a numeric and string value
        attrVal.numValue = numValue
        attrVal.strValue = strValue

        if session is None:  # commit if the session was created here
            self.commitWithRetry(sess)
            sess.close()

    def createOutput(self, name, description=None, program=DFLT_PROGRAM, session=None):
        '''
        Create an Output record with the given arguments unless the record
        exists for this name and program. Uses caller's session if provided. If no
        session is provided, the row is committed and the new outputId is returned.
        If the caller passes a session, None is returned unless the object was found,
        in which case the outputId is returned.
        '''
        sess      = session or self.Session()
        programId = self.getProgramId(program)
        outputId  = sess.query(Output.outputId).filter_by(programId=programId, name=name).scalar()

        if outputId is None:
            output = Output(name=name, programId=programId, description=description)
            sess.add(output)

            if not session:
                sess.commit()
                outputId = output.outputId
                sess.close()

        return outputId

    def getOutputIds(self, nameList, program=DFLT_PROGRAM, session=None):
        sess = session or self.Session()
        #programId = self.getProgramId(program) # TBD: use this
        ids = sess.query(Output.outputId).filter(Output.name.in_(nameList)).all()
        return zip(*ids)[0] if ids else []

    #
    # Very much like setAttrVal. So much so that perhaps the Result table can be
    # eliminated in favor of using the generic attribute/value system?
    #
    def setOutValue(self, runId, paramName, value, program=DFLT_PROGRAM, session=None):
        '''
        Set the given named output parameter to the given numeric value. Overwrite a
        previous value for this runId and attribute, if found, otherwise create a new value
        record. If session is not provided, one is allocated, and the transaction is
        committed. If a session is provided, the caller is responsible for calling commit.
        '''
        #_logger.debug('setOutValue(%s, %s, %s, session=%s', runId, paramName, value, session)
        sess = session or self.Session()

        outputId = sess.query(Output.outputId).filter_by(name=paramName).join(Program).filter_by(name=program).scalar()
        if not outputId:
            raise CoreMcsSystemError("%s output %s was not found in the Output table" % (program, paramName))

        results = sess.query(OutValue).filter_by(runId=runId, outputId=outputId).all()

        # If previous value is found, overwrite it; otherwise create a new one
        if results:
            #_logger.debug("setOutValue: updating value for outputId=%d" % outputId)
            #result = resultQuery.one()
            result = results[0]
            result.value = value
        else:
            #_logger.debug("setOutValue: adding value for outputId=%d" % outputId)
            sess.add(OutValue(runId=runId, outputId=outputId, value=value))

        if session is None:
            self.commitWithRetry(sess)
            sess.close()

    def getOutValues(self, simId, expName, outputName, limit=None):
        '''
        Return a dataframe (later maybe an iterator?) with columns trialNum and outputName,
        for the given sim, exp, and output variable.
        '''
        from pandas import DataFrame

        session = self.Session()

        limit = sys.maxint if limit is None or limit <= 0 else limit

        # This is essentially this query, but with "JOIN xx ON" syntax generated:
        #   select r.trialNum, v.value from run r, outvalue v, experiment e, output o
        #   where e.expName='test exp' and r.expid=e.expid and r.simid=1 and
        #         o.name='p1' and o.outputid=v.outputid;
        query = session.query(Run.trialNum).add_columns(OutValue.value).filter_by(simId=simId).\
        join(Experiment).filter_by(expName=expName).join(OutValue).join(Output).filter_by(name=outputName).\
        order_by(Run.trialNum).limit(limit)

        #print "getOutValues query: %s" % str(query.statement.compile())

        rslt = query.all()
        session.close()

        if not rslt:
            return None

        resultDF = DataFrame.from_records(rslt, columns=['trialNum', outputName], index='trialNum')
        return resultDF

    def deleteRunResults(self, runId, outputIds=None, session=None):
        sess = session or self.Session()

        query = sess.query(OutValue).filter_by(runId=runId)
        if outputIds:
            query = query.filter(OutValue.outputId.in_(outputIds))

        #query.delete(synchronize_session='fetch')
        query.delete(synchronize_session=False)

        if session is None:
            self.commitWithRetry(sess)
            sess.close()

    def queryToDataFrame(self, query):  # TBD: Not used anywhere yet...
        from pandas import DataFrame    # lazy import

        session = self.Session()
        result = session.execute(query)
        columnNames = result.keys()
        values = result.fetchall()
        session.close()
        return DataFrame(values, columns=columnNames)

    def getParameterValues(self, simId, program, asDataFrame=False):
        from pandas import DataFrame    # lazy import
        session = self.Session()

        query = session.query(InValue.row, InValue.col, InValue.value, InValue.trialNum, Input.paramName).\
                 filter(InValue.simId == simId).join(Input).join(Program).filter(Program.name == program).order_by(InValue.trialNum)

        rslt = query.all()
        cols = map(lambda d: d['name'], query.column_descriptions) if rslt else None
        session.close()

        if not rslt:
            return None

        if not asDataFrame:
            return rslt

        resultDF = DataFrame.from_records(rslt, columns=cols, index='trialNum')
        return resultDF

    def getParameters(self):
        session = self.Session()
        query = session.query(Input.paramName, InValue.row, InValue.col).\
            filter(Input.inputId == InValue.inputId).\
            distinct(Input.paramName, InValue.row, InValue.col)

        rslt = query.all()
        session.close()
        return rslt

    def getAppId(self, appName):
        session = self.Session()
        appId = session.query(Application).filter_by(appName=appName).scalar()
        session.close()
        return appId # N.B. scalar() returns None if no rows are found

    def currentAppId(self):
        if self.appId is not None:
            return self.appId

        cfg = getConfigManager()
        appName = cfg.getAppName()
        self.appId = self.getAppId(appName)
        return self.appId

    def createRun(self, simId, trialNum, jobNum=None, expName=None, expId=None, status=RUN_QUEUED, session=None):
        """
        Create an entry for a single model run, initially in "queued" state
        """
        assert (expName or expId), "Database createRun called with neither expName nor expId"

        sess = session or self.Session()
        if expId is None:
            exp = sess.query(Experiment.expId).filter_by(expName=expName).one()
            expId = exp.expId

        # if prior run record exists for this {simId, trialNum, expId} tuple, delete it
        sess.query(Run).filter_by(simId=simId, trialNum=trialNum, expId=expId).delete()

        run = Run(simId=simId, trialNum=trialNum, expId=expId, jobNum=jobNum, status=status)
        sess.add(run)

        if not session:     # if we created the session locally, commit; else call must do so
            self.commitWithRetry(sess)
            sess.close()

        return run

    def createRunFromContext(self, context, status=RUN_QUEUED, session=None):
        run = self.createRun(context.simId, context.trialNum, expName=context.expName,
                             status=status, session=session)
        _logger.debug("createRunFromContext returning runId %s", run.runId)
        return run

    def getRun(self, simId, trialNum, expName):
        session = self.Session()
        run = session.query(Run).filter_by(simId=simId, trialNum=trialNum).\
                join(Experiment).filter_by(expName=expName).scalar()

        session.close()
        return run    # N.B. scalar() returns None if no rows are found

    def getRunByRunId(self, runId):
        session = self.Session()
        run = session.query(Run).filter_by(runId=runId).scalar()
        session.close()
        return run    # scalar() returns None if no rows are found

    def getRunFromContext(self, context):
        run = self.getRun(context.simId, context.trialNum, context.expName)
        #_logger.debug("getRunIdFromContext returning runId %s", run.runId if run else None)
        return run

    def setRunStatus(self, runId, status, jobNum=None):
        '''
        Set the runStatus to the value for the given string and
        optionally set the job number.'''
        session = self.Session()
        try:
            run = session.query(Run).filter_by(runId=runId).one()
            run.status = status    # insert/update listener sets status code and timestamps
            if jobNum:
                run.jobNum = jobNum

            self.commitWithRetry(session)
            return run

        except NoResultFound:
            _logger.warn("setRunStatus failed to find record for runId %d", runId)
            return None

        finally:
            session.close()

    def getRunsWithStatus(self, simId, expList, status):
        from operator import itemgetter         # lazy import

        # Allow expList to be a single string, which we convert to a list
        if isinstance(expList, basestring):
            expList = [expList]

        session = self.Session()
        rslt = session.query(Run.trialNum).filter_by(simId=simId).join(Experiment).\
            filter(Run.status == status, Experiment.expName.in_(expList)).order_by(Run.trialNum).all()
        session.close()

        if rslt:
            rslt = map(itemgetter(0), rslt) # collapse list of singleton tuples into a single list

        _logger.debug("for simid=%d, expList=%s, status=%s, rslt=%s" % (simId, expList, status, rslt))
        return rslt


    def getFailedRuns(self, simId, expList):
        return self.getRunsWithStatus(simId, expList, RUN_FAILED)

    def getKilledRuns(self, simId, expList):
        return self.getRunsWithStatus(simId, expList, RUN_KILLED)

    def getQueuedRuns(self, simId, expList):
        return self.getRunsWithStatus(simId, expList, RUN_QUEUED)

    def getRunningRuns(self, simId, expList):
        return self.getRunsWithStatus(simId, expList, RUN_RUNNING)


    def createSim(self, trials, description, simId=None, updateTrials=False):
        '''
        Creates a new simulation with the given number of trials and description
        '''
        session = self.Session()
        if updateTrials:
            newSim = session.query(Sim).filter_by(simId=simId).one()
            newSim.trials = trials
        else:
            if simId is None:
                newSim = Sim(trials=trials, description=description)
            else:
                session.query(Sim).filter_by(simId=simId).delete()
                newSim = Sim(trials=trials, description=description, simId=simId)

            session.add(newSim)
            self.commitWithRetry(session)

        newSimId = newSim.simId
        session.close()
        return newSimId

    def getTrialCount(self, simId):
        session = self.Session()
        trialCount = session.query(Sim.trials).filter_by(simId=simId).scalar()
        session.close()
        return trialCount

    def createExp(self, name, description=None):
        'Insert a row for the given experiment'
        session = self.Session()
        exp = Experiment(expName=name, description=description)
        session.add(exp)
        self.commitWithRetry(session)
        expId = exp.expId
        session.close()
        return expId

    # TBD: filter by program name, too
    def getExpId(self, expName, session=None):
        exp = self.getExp(expName, session)
        return exp.expId

    def getExp(self, expName, session=None, raiseError=True):
        sess = session or self.Session()

        try:
            exp = sess.query(Experiment).filter_by(expName=expName).one()

        except NoResultFound:
            msg = "The experiment '%s' is not defined" % expName
            if raiseError:
                _logger.fatal(msg)
                raise CoreMcsUserError(msg)
            else:
                _logger.info(msg)
                exp = None

        finally:
            if session:
                session.close()

        return exp

    def getProgramId(self, program):
        session = self.Session()
        programId = session.query(Program.programId).filter_by(name=program).scalar()
        session.close()
        return programId


# Single instance of the class. Use 'getDatabase' constructor
# to ensure that this instance is returned if already created.
_DbInstance = None
_DbClass    = CoreDatabase


def setDatabaseClass(dbClass):
    '''
    Set the class to use for the one database instance.
    N.B. Has no effect once the instance has been allocated.
    '''
    global _DbClass
    _DbClass = dbClass


def getDatabase(dbClass=None, checkInit=True):
    '''
    Return the instantiated CoreDatabase, or created one and return it.
    The optional dbClass argument is provided to facilitate subclassing.
    '''
    global _DbInstance, _DbClass

    if _DbInstance is None:
        if dbClass is None:
            dbClass = _DbClass

        _DbInstance = dbClass()
        _DbInstance.startDb(checkInit=checkInit)

    return _DbInstance


def getSession():
    '''
    Convenience method for functions that need only a session, not the db object.
    If a subclass of CoreDatabase is required, getDatabase should be called the
    first time with that dbClass.
    '''
    db = getDatabase()
    return db.Session()


def dropTable(tableName, meta):
    if tableName in meta.tables:
        # Drop the table if it exists and remove it from the metadata
        table = meta.tables[tableName]
        table.drop()
        meta.remove(table)

#
# Below are functions meant to be used by the program or function invoked by Runner.
# All simIds, trialNums, expNames are the current simId, trialNum, and expName
#

def getContextRunId():
    'Return the runId associated with the current context'
    db  = getDatabase()
    ctx = U.getContext()
    run = db.getRunFromContext(ctx)
    return run.runId


def setOutValue(outputName, value, program=DFLT_PROGRAM, session=None):
    '''
    Record the numeric result of user's program named "program".
    Optional session arg facilitates writing multiple results at once.
    '''
    db    = getDatabase()
    runId = getContextRunId()
    db.setOutValue(runId, outputName, value, program, session)
