'''
Created on Nov 6, 2012

@author: Rich Plevin
@author: Sam Fendell

Copyright (c) 2012-2015. The Regents of the University of California (Regents)
and Richard Plevin. See the file COPYRIGHT.txt for details.
'''
import re
from collections import defaultdict
from .error import CoreMcsUserError
from .util import fileReader, BaseSpecError
from .Constants import SETFILE_SUFFIX, DICT_SEPARATOR, DICT_REGEX


class SymSetSpecError(BaseSpecError):
    pass

class SymSetError(CoreMcsUserError):
    pass

class SymSet(object):
    '''
    Stores a named list containing the given elements, and provides
    lookup of index by name.
    '''

    # Stores all the defined SymSet instances
    _SymsetDict = defaultdict(lambda: None)

    def __init__(self, name, symbols, setType='int', start=1, extraSymbols=None):
        self.name = name.upper()    # TBD: should symbols be case-insensitive?
        self.symbols = symbols
        self.start   = start
        self.type    = setType
        self.extraSymbols = extraSymbols or {}
        self.__class__._SymsetDict[self.name] = self

    def __str__(self):
        classname = type(self).__name__
        return '<%s %s=%s>' % (classname, self.name, self.symbols)

    def __getitem__(self, key):
        'Allow access using set[key] syntax'
        return self.value(key.upper())

    def __iter__(self):
        return iter(self.symbols)

    @classmethod
    def clearSets(cls):
        '''
        Deletes all SymSets that have been read in.
        '''
        cls._SymsetDict = defaultdict(lambda: None)

    @classmethod
    def getSetList(cls):
        return cls._SymsetDict.values()

    @classmethod
    def symbolValue(cls, setName, symbol):
        '''
        Gets the index associated with the symbol from the given setName.
        Convenience method; identical to SymSet.get(setName).value(symbol)
        setName: String naming a SymSet.
        symbol: name of a symbol
        Returns integer
        '''
        setName = setName.upper()
        return cls.get(setName).value(symbol)

    @classmethod
    def get(cls, setName, raiseError=True):
        '''
        Gets the symbolic set associated with the given name.
        setName: string
        Returns SymSet instance.
        '''
        setName = setName.upper()
        symSet  = cls._SymsetDict[setName]
        if symSet:
            return symSet

        if raiseError:
            raise SymSetError('SymSet has no set named "%s"' % setName)
        else:
            return None

    @classmethod
    def createAlias(cls, alias, original):
        '''
        Clone a set under a new name.
        :param original: the name of the original set
        :param alias:  the alias to assign to the same underlying set
        :return: none
        '''
        alias = alias.upper()
        s = cls.get(original)
        return cls(alias, s.symbols, setType=s.type, start=s.start, extraSymbols=s.extraSymbols)

    # TBD: simplify this
    @classmethod
    def readFile(cls, filename):
        '''
        Read a file of lines consisting of whitespace-delimited strings, where the first
        string is the set name and the rest are the elements, in the desired order. If a
        term like start=<int> is present, this value is used as the starting value for the
        set. Examples:

            name=LATITUDE        boreal    temperate    tropical
            name=SPECIES        CO2    CO    CH4    N2O    NMHC
            name=forestBurningEF type=symbol LATITUDE SPECIES
        '''
        setList = []
        argDict = {}
        extraSymbols = {}
        syms = []
        name = False
        for items in fileReader(filename, error=SymSetSpecError, fileExtension=SETFILE_SUFFIX):
            for item in items:
                item = item.upper()
                if re.match(DICT_REGEX, item):
                    key, value = item.split(DICT_SEPARATOR)
                    if key == 'START':
                        argDict['start'] = int(value)
                    elif key == 'NAME':
                        if name:
                            setList.append(cls(name, syms, **argDict))
                        extraSymbols = {}
                        argDict = {}
                        syms = []
                        name = value
                    elif key == 'TYPE':
                        argDict['setType'] = value
                    else:
                        if not name:
                            raise SymSetSpecError('Extra symbol found before name was given.')

                        extraSymbols[key] = int(value)
                        argDict['extraSymbols'] = extraSymbols

                elif DICT_SEPARATOR in item:
                    raise SymSetSpecError('Malformed dict statement.')

                else:
                    if not name:
                        raise SymSetSpecError('Symbol found before name was given.')
                    syms.append(item)

        if name:
            setList.append(cls(name, syms, **argDict))

        #print("Set list: \n  %s" % '\n  '.join(map(str, setList)))
        return setList


    @classmethod
    def writeFile(cls, filename, setList):
        '''
        :param filename: the name of the file to create
        :param setList: a list of SymSet instances to write to the file
        :return: nothing
        '''
        with open(filename, 'w') as f:
            for symset in setList:
                f.write("%s\n" % symset.strFormat())


    def key(self, index):
        '''
        Returns the symbol name for the given index
        '''
        return self.symbols[index - self.start]


    # TODO: create an iterator, e.g., "for x in mySet:"
    def getSymbols(self):
        return self.symbols


    def value(self, symbol):
        '''
        Takes in a symbol name, and returns the index associated with it. If two indices were
        given for a symbol, the default is the one given without using the 'equals' notation.
        '''
        symbol = symbol.upper()

        if self.type == 'int':
            if symbol in self.symbols:
                return self.symbols.index(symbol) + self.start

            if symbol in self.extraSymbols.keys():
                return self.extraSymbols[symbol]

            raise SymSetError('SymSet "%s" has no member "%s"' % (self.name, symbol))

        elif self.type == 'symbol':
            return SymSet.get(symbol)


    def range(self):
        '''
        Returns a tuple of the start and end indices
        '''
        return self.start, self.start + len(self.symbols)


    def count(self):
        '''
        Returns number of symbols in the set
        '''
        return len(self.symbols)

    # Make "rank" an alias for "count"
    def rank(self):
        return len(self.symbols)


    def strFormat(self):
        '''
        :return: a string that can be read by SymSet.readFile() to reproduce this set
        '''
        s  = "name=%s" % self.name
        s += (" type=symbol" if self.type == 'symbol' else "")
        s += (" start=%d" % self.start if self.start != 1 else "")
        s += " "
        s += " ".join(self.symbols)

        if len(self.extraSymbols) > 0:
            extras = map(lambda pair: "%s=%d" % (pair[0], pair[1]), self.extraSymbols.iteritems())
            s += " %s" % " ".join(extras)

        return s

